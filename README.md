# Sudoku Engine

## About This Project

This project implements a Sudoku solver, an application that analyzes a given 
Sudoku puzzle and outputs a report of steps that may be used to solve it.


## About Sudoku

Sudoku is a logic-based, combinatorial number-placement puzzle. The objective is to fill 
a 9�9 grid with digits so that each column, each row, and each of the nine 3�3 subgrids that 
compose the grid contain all of the digits from 1 to 9.  The puzzle setter provides a partially 
completed grid, which for a well-posed puzzle has a single solution.  (Wikipedia)


## User Story: Create Sudoku Solver

As a **Sudoku enthusiast** who has a Sudoku puzzle to solve,  
I want a **discover the steps to solve my puzzle**  
So that I can **learn how to solve my puzzle**, and also **learn how to solve other Sudoku puzzles**.


#### Acceptance Criteria

- **Given** a particular Sudoku puzzle  
  **When** I ask the program to solve my puzzle  
  **Then** it provides a valid step-by-step procedure for arriving at a solution

- **Given** a particular Sudoku puzzle and the program's step-by-step solution  
  **When** desired  
  **Then** the step-by-step solution can be followed to manually solve the puzzle

- **Given** a particular Sudoku puzzle and the program's step-by-step solution  
  **When** desired  
  **Then** the step-by-step solution can be validated by analysing the logic and actions of each step.


## Tools / Technical Stack

- Visual Studio 2022 (Community Edition)
- .NET 7
- NUnit

### Optional

- NCrunch for Visual Studio 2022 (4.18.*)


## Repository Layout

| File / Directory            | Description                                                           |
| :-------------------------- | :-------------------------------------------------------------------- |
| Sudoku.sln                  | Visual Studio solution encompassing all projects/code                 |  
| Sudoku.SolverConsole/       | A console application that will solve a specified Sudoku puzzle       |  
| Sudoku.SolverConsole.Tests/ | Unit tests for Sudoku.SolverConsole classes                           |  
| Sudoku.Engine/              | A class library containing the core models, logic, and solver         |  
| Sudoku.Engine.Tests/        | Unit tests for Sudoku.Engine classes                                  |  
