using Sudoku.Engine;

namespace Sudoku.Engine.Tests;

public class SampleClassTests
{
    [Test]
    public void Constructor_Default_ThrowsNotImplemented()
    {
        // arrange

        // act
        TestDelegate act = () => { var i = new SampleClass(); };

        // assert
        Assert.Throws<NotImplementedException>(act);
    }
}